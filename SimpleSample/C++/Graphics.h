#pragma once


#include <d3d11.h>

#include <directxmath.h>

#include "CommonStates.h"
#include "DDSTextureLoader.h"
#include "SpriteBatch.h"
#include "SpriteFont.h"

using namespace DirectX;

class Graphics
{
private:
	D3D_DRIVER_TYPE                     g_driverType;
	D3D_FEATURE_LEVEL                   g_featureLevel;

	ID3D11Device*                       g_pd3dDevice;
	ID3D11DeviceContext*                g_pImmediateContext;
	IDXGISwapChain*                     g_pSwapChain;
	ID3D11RenderTargetView*             g_pRenderTargetView;
	ID3D11Texture2D*                    g_pDepthStencil;
	ID3D11DepthStencilView*             g_pDepthStencilView;

	ID3D11InputLayout*                  g_pBatchInputLayout;

	std::unique_ptr<SpriteBatch>        g_Sprites;
	std::unique_ptr<SpriteFont>         g_Font;

public:
	Graphics(void);
	~Graphics(void);

	HRESULT InitDevice(HWND hWnd);

	ID3D11DeviceContext & GetContext() const { return *g_pImmediateContext; };
	ID3D11Device & GetDevice() const { return *g_pd3dDevice; };

	void ClearRenderTarget();
	void BeginSpriteDrawing();
	void EndSpriteDrawing();

	void DrawString(_In_z_ wchar_t const* text, DirectX::XMFLOAT2 const& position, FXMVECTOR color);
	void Draw(_In_ ID3D11ShaderResourceView* texture, XMFLOAT2 const& position, FXMVECTOR color = Colors::White);

	void Present();
};

